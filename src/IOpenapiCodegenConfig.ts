import { ICodegenOption } from "./models/ICodegenOption";

export interface IOpenapiCodegenConfig {
  path: string;
  openApiVersion: "v2" | "v3";
  modelOption?: ICodegenOption;
  requestModelOption?: ICodegenOption;
}