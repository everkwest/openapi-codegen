declare module 'swagger2openapi' {
  export function convertUrl(url: string, options: any, callback: (options: any, err: any) => any): void;
}