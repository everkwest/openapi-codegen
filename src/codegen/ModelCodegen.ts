import { Project, StructureKind, Scope, PropertySignatureStructure, ImportDeclarationStructure, InterfaceDeclarationStructure, OptionalKind, CodeBlockWriter, TypeAliasDeclarationStructure, VariableDeclarationKind, VariableStatementStructure, ImportSpecifierStructure } from "ts-morph";
import { camel, pascal } from "change-case";
import _ from "lodash";
import { emptyDirSync } from "fs-extra";
import IApiModel from "../models/IApiModel";
import { ICodegenOption, ITypeReplacer } from "../models/ICodegenOption";
import { ModelType } from "../models/Types";

export default class ModelCodegen {
  public static createModelFile(models: IApiModel, option: ICodegenOption) {

    if(option.cleanOutDir) {
      emptyDirSync(option.outDir);
    }

    const project = new Project();

    const modelNames = Object.keys(models);
    modelNames.forEach(modelName => {
      const model = models[modelName];
      const outDir = option.outDir.endsWith('/') ? option.outDir : option.outDir + '/';
      const imports: {[importName: string]: OptionalKind<ImportDeclarationStructure>} = {};
      const interfaceModels: OptionalKind<InterfaceDeclarationStructure>[] = [];
      const enums: {[enumName: string]: any[]} = {};

      const sf = project.createSourceFile(`${outDir}${option.fileCamelCase ? camel(modelName) : pascal(modelName)}.ts`, undefined, {overwrite: true});

      if (model.type === "object") {
        const interfaceModel: InterfaceDeclarationStructure = {
          name: modelName,
          kind: StructureKind.Interface,
          isExported: true,
          properties: []
        };
        interfaceModels.push(interfaceModel);

        if (model.properties) {
          const properties = Object.keys(model.properties);
          properties.forEach(propertyName => {
            const property = model.properties![propertyName];
            const propertyType = this.getType(property, option);
            const propertyModel: OptionalKind<PropertySignatureStructure> = {
              name: `"${propertyName}"`,
              type: propertyType.typeName,
              hasQuestionToken: !model.required || !model.required.some(x => x === propertyName)
            };

            if(propertyType.enum) {
              const enumName = `${pascal(propertyName)}Enum`;
              propertyModel.type = `${modelName}.${enumName}`;
              enums[enumName] = propertyType.enum;
            }

            if(propertyType.import) {
              if(imports[propertyType.import.moduleSpecifier]) {
                const oldImport = imports[propertyType.import.moduleSpecifier];
                const newImport = propertyType.import;

                if(!oldImport.defaultImport){
                  oldImport.defaultImport = newImport.defaultImport;
                }

                if(!oldImport.namedImports){
                  oldImport.namedImports = newImport.namedImports;
                } else {
                  const unionImports = [...oldImport.namedImports as OptionalKind<ImportSpecifierStructure>[] , ...newImport.namedImports as OptionalKind<ImportSpecifierStructure>[]];
                  oldImport.namedImports = _.uniqBy(unionImports, x => x.name);
                }

              } else {
                imports[propertyType.import.moduleSpecifier] = propertyType.import;
              }
            }
            interfaceModel.properties!.push(propertyModel);

          });
        }
      }
      sf.addImportDeclarations(Object.values(imports));
      sf.addInterfaces(interfaceModels);

      const enumNames = Object.keys(enums);
      if(enumNames.length > 0) {

        const enumTypes: TypeAliasDeclarationStructure[] = enumNames.map(n => ({
          name: n,
          isExported: true,
          type: (writer: CodeBlockWriter) => ModelCodegen.writeUnionType(writer, enums[n]),
          kind: StructureKind.TypeAlias as StructureKind.TypeAlias
        }));

        const enumObject: VariableStatementStructure[] = enumNames.map(n => ({
          isExported: true,
          kind: StructureKind.VariableStatement as StructureKind.VariableStatement,
          declarationKind: VariableDeclarationKind.Const,
          declarations: [{
            name: n,
            initializer: (w: CodeBlockWriter) => {
              w.block(() => ModelCodegen.writeVariable(w, enums[n]))
            }
          }]
        }));

        sf.addNamespace({
          name: modelName,
          isExported: true,
          statements: [
            ...enumTypes,
            ...enumObject
          ]
        });
      }

      sf.save();
    });
  }

  private static getType(property: ModelType, option: ICodegenOption): {
    typeName: string,
    import?: OptionalKind<ImportDeclarationStructure>,
    enum?: any[]
  } {
    const replacer = option.customTypeMapper || {};
     if (property.type === "ref") {
      if (replacer[property.$ref]) {
        return {
          typeName: replacer[property.$ref].destType,
          import: replacer[property.$ref].importFrom ? {
            moduleSpecifier: replacer[property.$ref].importFrom!.moduleSpecifier,
            namedImports: replacer[property.$ref].importFrom!.namedImports,
            defaultImport: replacer[property.$ref].importFrom!.defaultImport
          } : undefined
        }
      }
      return {
        typeName: property.$ref,
        import: {
          moduleSpecifier: `./${option.fileCamelCase ? camel(property.$ref) : pascal(property.$ref)}`,
          namedImports: [{
            name: property.$ref
          }]
        }
      };
    } else if (property.type === "array") {
      const arrayType = ModelCodegen.getType(property.items, option);
      return {
        typeName: `Array<${arrayType.typeName}>`,
        import: arrayType.import,
        enum: arrayType.enum
      }
    } else if (property.type === "integer") {
      return {
        typeName: "number"
      }
    } else {
      if(property.enum) {
        return {
          typeName: '',
          enum: property.enum
        }
      }
      return {
        typeName: property.type,
      }
    }
  }

  private static writeUnionType(writer: CodeBlockWriter, types: string[]) {
    types.forEach((type, idx) => {
      writer.quote();
      writer.write(type);
      writer.quote();

      if(idx < (types.length - 1)) {
        writer.space();
        writer.write("|");
        writer.space();
      }
    });
  }

  private static writeVariable(writer: CodeBlockWriter, types: string[]) {
    types.forEach((type, idx) => {
      writer.quote();
      writer.write(type);
      writer.quote();

      writer.write(":");
      writer.space();

      writer.quote();
      writer.write(type);
      writer.quote();
      writer.write(",");
      writer.newLine();
    });
  }

}