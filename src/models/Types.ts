export type NonArrayModelType = 'null' | 'boolean' | 'object' | 'number' | 'string' | 'integer';
export type ArrayModelType = 'array';
export type RefModelType = 'ref';

export type ModelType = IArrayModel | INonArrayModel | IReferenceModel;

export interface IArrayModel extends IModelBase {
  type: ArrayModelType;
  items: IReferenceModel | ModelType;
}
export interface INonArrayModel extends IModelBase {
  type: NonArrayModelType;
}

export interface IReferenceModel {
  type: RefModelType;
  $ref: string;
}

export interface IModelBase {
  title?: string;
  description?: string;
  format?: string;
  default?: any;
  maximum?: number;
  exclusiveMaximum?: boolean;
  minimum?: number;
  exclusiveMinimum?: boolean;
  maxLength?: number;
  minLength?: number;
  pattern?: string;
  maxItems?: number;
  minItems?: number;
  uniqueItems?: boolean;
  maxProperties?: number;
  minProperties?: number;
  required?: string[];
  enum?: any[];
  properties?: {
      [name: string]: ModelType;
  };
  nullable?: boolean;
  readOnly?: boolean;
  writeOnly?: boolean;
  example?: any;
  deprecated?: boolean;
}