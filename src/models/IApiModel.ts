import { ModelType } from "./Types";

export default interface IApiModel {
  [name: string]: ModelType;
}