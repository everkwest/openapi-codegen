/// <reference path="../types/swagger2openapi.d.ts" />
import converter from "swagger2openapi";
import { IOpenapiProvider } from "./IOpenapiProvider";
import { BaseProvider } from "./BaseProvider";
import { OpenAPIV3 } from "openapi-types";

export default class SwaggerProvider extends BaseProvider implements IOpenapiProvider {
  constructor(
    swagger: OpenAPIV3.Document
  ) {
    super(swagger);
  }

  public static async getInstance(path: string): Promise<IOpenapiProvider> {
    const promise = new Promise<IOpenapiProvider>((resolve, reject) => {
      converter.convertUrl(path, {}, (op, err) => {
        if(err){
          reject(err);
        }
        const provider = new SwaggerProvider(op.options.openapi);
        resolve(provider);
      })
    });

    return promise;
  }
}