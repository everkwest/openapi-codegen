import SwaggerParser from "swagger-parser";
import { IOpenapiProvider } from "./IOpenapiProvider";
import IApiModel from "../models/IApiModel";
import { BaseProvider } from "./BaseProvider";
import { OpenAPIV3, OpenAPI } from "openapi-types";

export default class OpenapiProvider extends BaseProvider implements IOpenapiProvider {
  constructor(
    swagger: OpenAPIV3.Document
  ) {
    super(swagger);
  }

  public static async getInstance(path: string): Promise<IOpenapiProvider> {
    const swagger = await SwaggerParser.parse(path, { dereference: { circular: false } });
    if (OpenapiProvider.isOpenapi(swagger)) {
      return new OpenapiProvider(swagger);
    } else {
      throw new Error('Document is not OpenApiV3');
    }
  }

  private static isOpenapi(document: OpenAPI.Document): document is OpenAPIV3.Document {
    return (document as any).openapi;
  }

}