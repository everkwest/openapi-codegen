import IApiModel from "../models/IApiModel";

export interface IOpenapiProvider {
  getRequestModel: () => Promise<IApiModel>;
  getResponseModel: () => Promise<IApiModel>;
}