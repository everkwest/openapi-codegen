import { OpenAPI, OpenAPIV3 } from "openapi-types";
import { ModelType } from "../models/Types";
import { pascal } from "change-case";
import IApiModel from "../models/IApiModel";

export class BaseProvider {

  constructor(
    private swagger: OpenAPIV3.Document
  ) {
  }

  public async getResponseModel() {
    let res: IApiModel = {};
    if (this.swagger.components && this.swagger.components.schemas) {
      res = this.getApiModel(this.swagger.components.schemas, this.normalizeName);
    }
    return res;
  }
  public async getRequestModel() {
    return this.getApiModelFromPath(this.swagger.paths);
  }

  protected isRefObject(node: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject | OpenAPIV3.ParameterObject): node is OpenAPIV3.ReferenceObject {
    return (node as any)['$ref'];
  }

  protected isArraySchemaObject(node: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject): node is OpenAPIV3.ArraySchemaObject {
    return (node as any)['type'] === 'array';
  }

  protected isNonArraySchemaObject(node: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject): node is OpenAPIV3.NonArraySchemaObject {
    return (node as any)['type'] !== 'array';
  }

  protected nodeToModel(node: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject): ModelType {
    if (this.isRefObject(node)) {
      return {
        type: "ref",
        $ref: this.normalizeName(this.extractId(node.$ref))
      };
    } else if (this.isNonArraySchemaObject(node)) {
      const properties = node.properties ? this.getApiModel(node.properties, this.normalizePropertyName) : {};
      return {
        ...node,
        properties
      };
    } else {
      return {
        type: 'array',
        items: this.nodeToModel(node.items)
      };
    }
  }

  protected getApiModel(obj: { [key: string]: OpenAPIV3.ReferenceObject | OpenAPIV3.SchemaObject; }, normalizeName: (val: string) => string) {
    const res: IApiModel = {};
    const keys = Object.keys(obj);
    keys.forEach(elName => {
      const schema = obj[elName];
      const name = normalizeName(elName);
      res[name] = this.nodeToModel(schema);
    });
    return res;
  }

  protected getApiModelFromPath(pathsObject: OpenAPIV3.PathsObject) {
    const res: IApiModel = {};
    const paths = Object.keys(pathsObject);
    paths.forEach(path => {
      const pathObject = pathsObject[path];

      const name = this.pathAndMethodToName(path, "get");
      const requestModel: ModelType = { type: "object", required: [], properties: undefined };
      if (pathObject.get && pathObject.get.parameters) {
        pathObject.get.parameters.forEach(parameter => {
          if (!this.isRefObject(parameter) && parameter.in === "query" && parameter.schema) {
            if (!requestModel.properties) {
              requestModel.properties = {};
            }
            if (parameter.required) {
              requestModel.required!.push(parameter.name);
            }
            requestModel.properties![parameter.name] = this.nodeToModel(parameter.schema);
          }
        });
      }
      if (requestModel.properties) {
        res[name] = requestModel;
      }
    });
    return res;
  }

  protected pathAndMethodToName(path: string, method: string) {
    const name = `${path}-${method}RequestModel`.replace(/{/g, "-by-");
    return pascal(name);
  }


  protected normalizeName(value: string) {
    return value.replace(/[^a-zA-Z0-9]/g, '');
  }

  protected normalizePropertyName(value: string) {
    return value.replace(/[^a-zA-Z0-9_]/g, '');
  }

  protected extractId(value: string) {
    return value.replace('#/components/schemas/', '');
  }
}